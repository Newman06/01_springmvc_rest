package fr.adaming.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@XmlRootElement// cette annotation permet la serialisation des oblejet de cette classe en XML
//@XmlAccessorType(XmlAccessType.FIELD)// pemert de specifier à JAXB que les annotation se trouvent sur les attributs

@Entity
@Table(name = "employes")
public class Employe {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	// @XmlElement(name = "pseudo")
	private String nom;
	private String prenom;

	// @XmlTransient // pour ignorer cet attribut lors de la serialisation en XML
	// (annotation de JAXB)
	// @JsonIgnore // pour ignorer cet attribut lors de la serialisation en JSON
	// (annotation de JACKSON)
	private int age;

	public Employe() {
		super();
	}

	public Employe(String nom, String prenom, int age) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}

	public Employe(int id, String nom, String prenom, int age) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Employe [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", age=" + age + "]";
	}

}

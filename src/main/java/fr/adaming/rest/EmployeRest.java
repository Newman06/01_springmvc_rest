package fr.adaming.rest;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.adaming.model.Employe;
import fr.adaming.service.IEmployeService;

@RestController // cette annotation permet de specifier que cette classe est u web service
				// restFul developpé par spring MVC rest
@RequestMapping("/employe")

public class EmployeRest {

	@Autowired
	private IEmployeService eService;

	@RequestMapping(value = "/find", method = RequestMethod.GET, produces = { "application/json", "application/xml" })
	public Employe recupererEmploye(@RequestParam("pId") int id) {

		return eService.getEmployeById(id);
	}

	@GetMapping(value = "/liste", produces = "application/json")
	public List<Employe> recuprerListe() {
		return eService.getAllEmploye();
	}

	@PostMapping(value = "/add", produces = "application/json", consumes = "application/json")
	public Employe ajouterEmploye(@RequestBody Employe emp) {

		return eService.addEmploye(emp);
	}

	@PutMapping(value = "/update", produces = "application/json", consumes = "application/json")
	public Employe modifEmploye(@RequestBody Employe emp) {

		return eService.updateEmploye(emp);
	}

	@DeleteMapping(value = "/delete/{pId}")
	public void supprimerEmploye(@PathVariable("pId") int id) {
		eService.deleteEmploye(id);
	}

	@GetMapping(value = "/get/{pNom}/{pPrenom}", produces = "application/json")
	public Employe recupEmployeParNom(@PathVariable("pNom") String nom, @PathVariable("pPrenom") String prenom) {
		return eService.getEmployeByName(nom, prenom);
	}

	
	@GetMapping(value="/search/{pAge}", produces = "application/json")
	public List<Employe> recupEmployeParAge(@PathVariable("pAge")int age) {
		return eService.getEmployeByAge(age);
	}

}

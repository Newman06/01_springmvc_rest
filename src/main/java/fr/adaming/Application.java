package fr.adaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication // cette annotation permet d'auto-configurer mon projet: elle contient les 3
						// annotations suivant @Configuration, @EnableAutoConfiguration et
						// @ComponentScan
public class Application {

	public static void main(String[] args) {
		// la methode run permet de lancer un projet spring boot et recuperer le context
		// spring IoC ApplicationContext
		SpringApplication.run(Application.class, args);
	}

}

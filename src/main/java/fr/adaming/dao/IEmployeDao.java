package fr.adaming.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.adaming.model.Employe;


@Repository
public interface IEmployeDao extends JpaRepository<Employe, Integer> {
	
	// c'est une methode de requete 
	public Employe findByNomAndPrenom(String pNom,String pPrenom);
	
	
	@Query("SELECT e FROM Employe as e WHERE e.age >=:pAge")
	public List<Employe> searchByAge(@Param("pAge")int age);

}

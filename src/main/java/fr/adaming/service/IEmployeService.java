package fr.adaming.service;

import java.util.List;

import fr.adaming.model.Employe;

public interface IEmployeService {
	
	public Employe getEmployeById(int id);
	
	public List<Employe> getAllEmploye();
	
	public Employe addEmploye(Employe emp);
	
	public Employe updateEmploye(Employe emp);
	
	public void deleteEmploye(int id);
	
	public Employe getEmployeByName(String nom,String prenom);
	
	public List<Employe> getEmployeByAge(int age);
	

}

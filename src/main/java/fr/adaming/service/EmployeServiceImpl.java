package fr.adaming.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IEmployeDao;
import fr.adaming.model.Employe;

@Service
public class EmployeServiceImpl implements IEmployeService {

	@Autowired
	private IEmployeDao empDao;

	@Override
	public Employe getEmployeById(int id) {

		return empDao.findById(id).get();

	}

	@Override
	public List<Employe> getAllEmploye() {

		return empDao.findAll();
	}

	@Override
	public Employe addEmploye(Employe emp) {
		
		return empDao.save(emp);
	}

	@Override
	public Employe updateEmploye(Employe emp) {

		return empDao.save(emp);
	}

	@Override
	public void deleteEmploye(int id) {
		empDao.deleteById(id);
		
	}

	@Override
	public Employe getEmployeByName(String nom, String prenom) {

		return empDao.findByNomAndPrenom(nom, prenom);
	}

	@Override
	public List<Employe> getEmployeByAge(int age) {
		
		return empDao.searchByAge(age);
	}
	
	

}
